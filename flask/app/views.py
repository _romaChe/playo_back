from app import app
import os

@app.route("/")
def index():
    app_name = os.getenv("APP_NAME")

    if app_name:
        return "Hello from Docker!"

    return "Hello from Flask"
